function updateBadge (matchCount, domains) {
  const listed = domains.some(d => window.location.hostname.endsWith(d))
  const status = browser.i18n.getMessage(listed ? 'badgeOff' : 'badgeOn')

  browser.runtime.sendMessage({
    message: 'badge',
    text: matchCount > 0 ? `${matchCount}` : '',
    title: status
  })
}

utils.getDefaultDomains().then(gafamHosts => {
  let matchCount = 0
  let on = true
  let showNotif = false

  browser.storage.local.get({
    mainStyle: utils.defaultStyle,
    visitedStyle: null,
    useVisitedStyle: false,
    domains: gafamHosts,
    showTooltip: false
  }).then(conf => {
    showNotif = conf.showTooltip
    browser.runtime.sendMessage({ message: 'history' }).then(res => {
      if (conf.useVisitedStyle && conf.visitedStyle) {
        color(l => !res.history.some(h => h.url === l.href), conf.mainStyle, conf.domains)
        color(l => res.history.some(h => h.url === l.href), conf.visitedStyle, conf.domains)
      } else {
        color(_ => true, conf.mainStyle, conf.domains)
      }
    })
  })

  function color (filter, style, domains) {
    const links = Array.prototype.slice.call(document.querySelectorAll('a')).filter(filter)
    links.forEach(highlight(style, domains))

    browser.runtime.onMessage.addListener(msg => {
      if (msg.message === 'toggle') {
        matchCount = 0
        on = !on
        links.forEach(highlight(style, domains))
      }
    })

    const observer = new MutationObserver(muts => {
      for (const mut of muts) {
        mut.addedNodes.forEach(node => {
          highlight(style, domains)(node)
          links.push(node)
          try {
            const newLinks = Array.prototype.slice.call(node.querySelectorAll('a'))
            newLinks.forEach(l => links.push(l))
            newLinks.filter(filter).forEach(highlight(style, domains))
          } catch (e) {
            console.error(node, e)
          }
        })
      }
    })

    observer.observe(document.body, {
      attributes: true,
      childList: true,
      subtree: true,
      attributeFilter: [ 'href' ]
    })
  }

  function highlight (conf, domains) {
    return link => {
      if (link.hostname && link.hostname !== window.location.hostname) {
        if (domains.some(h => link.hostname.endsWith(h)) && on) {
          utils.styleLink(link, conf)
          updateBadge(matchCount++, domains)
          if (showNotif) {
            link.addEventListener('click', linkClicked)
          }
        } else if (link.style.cssText) { // reset style if needed
          link.style.cssText = ''
          updateBadge(matchCount--, domains)
          if (showNotif) {
            link.removeEventListener('click', linkClicked)
          }
        }
      }
    }
  }
})

function linkClicked (evt) {
  browser.runtime.sendMessage({ message: 'notification',
    domain: evt.target.hostname,
    data: {
      type: 'basic',
      title: browser.i18n.getMessage('proprietaryService', evt.target.hostname),
      message: browser.i18n.getMessage('libreAlt'),
      iconUrl: 'assets/icons/gafalink_64.png'
    }
  })
}
