browser.browserAction.onClicked.addListener(tab => {
  browser.tabs.sendMessage(tab.id, {
    message: 'toggle'
  })
})

browser.tabs.onUpdated.addListener((id, changes, tab) => {
  if (tab.url && !tab.url.startsWith('about:')) {
    browser.browserAction.enable(id)
  } else {
    browser.browserAction.disable(id)
  }
})

browser.runtime.onMessage.addListener((msg, { tab }) => {
  if (msg.message === 'badge') {
    browser.browserAction.setBadgeText({
      text: msg.text,
      tabId: tab.id
    })

    browser.browserAction.setTitle({
      title: msg.title,
      tabId: tab.id
    })
  }
})
