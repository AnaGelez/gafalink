browser.runtime.onMessage.addListener(msg => {
  if (msg.message === 'history') {
    return new Promise(resolve => {
      browser.history.search({ text: '', startTime: 0 }).then(history => {
        resolve({ history: history })
      })
    })
  }
})
