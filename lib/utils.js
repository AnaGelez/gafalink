// eslint-disable-next-line no-unused-vars
const utils = {
  defaultStyle: {
    color: '#FFFF00',
    backgroundColor: '#FF0000',
    opacity: '1',
    bold: false,
    italic: false,
    fontFamily: '',
    decoration: 'none',
    size: 1
  },

  getDefaultDomains: function () {
    return new Promise(resolve =>
      fetch(browser.extension.getURL('assets/default-domains.json'))
        .then(res => res.json())
        .then(res => resolve(res.domains))
    )
  },

  styleLink: function (elt, conf) {
    elt.style = elt.style || {}
    Object.assign(elt.style, {
      color: conf.color,
      backgroundColor: conf.backgroundColor,
      opacity: conf.opacity,
      fontFamily: conf.fontFamily !== '' ? conf.fontFamily : null,
      fontWeight: conf.bold ? 'bold' : null,
      fontStyle: conf.italic ? 'italic' : null,
      textDecoration: conf.decoration,
      fontSize: `${conf.fontSize}em`
    })
  },

  confToStyle: function (prop, val) {
    switch (prop) {
      case 'bold':
      case 'italic':
        return [ prop === 'bold' ? 'font-weight' : 'font-style', val ? prop : null ]
      case 'fontSize':
        return [ prop, `${val}em` ]
      case 'fontFamily':
        return [ prop, val !== '' ? val : null ]
      case 'decoration':
        return [ 'text-decoration', val ]
      default:
        return [ prop, val ]
    }
  },

  buildStyleForm: function (name, _onChange, style) {
    const example = this.h('a', { href: '#' }, [ this.t(browser.i18n.getMessage('exampleLink')) ])
    const onChange = _onChange.bind(null, example)

    for (const key in style) { // load default style
      onChange(key, style[key])
    }

    return this.h('div', {}, [
      this.h('h2', {}, [ this.t(name) ]),
      this.h('h3', {}, [ this.t(browser.i18n.getMessage('colors')) ]),
      this.h('label', {}, [
        this.t(browser.i18n.getMessage('textColor')),
        this.h('input', { type: 'color', name: 'color', value: style.color }, [], { change: onChange })
      ]),
      this.h('label', {}, [
        this.t(browser.i18n.getMessage('backgroundColor')),
        this.h('input', { type: 'color', name: 'backgroundColor', value: style.backgroundColor }, [], { change: onChange })
      ]),
      this.h('label', {}, [
        this.t(browser.i18n.getMessage('opacity')),
        this.h('input', { type: 'range', min: '0.01', max: '1', step: '0.01', name: 'opacity', value: style.opacity }, [], { change: onChange })
      ]),

      this.h('h3', {}, [ this.t(browser.i18n.getMessage('font')) ]),
      this.h('label', {}, [
        this.t(browser.i18n.getMessage('fontFamily')),
        this.h('input', { type: 'text', name: 'fontFamily', value: style.fontFamily }, [], { change: onChange })
      ]),
      this.h('label', {}, [
        this.h('input', { type: 'checkbox', name: 'bold', checked: style.bold }, [], { change: onChange }),
        this.t(browser.i18n.getMessage('bold'))
      ]),
      this.h('label', {}, [
        this.h('input', { type: 'checkbox', name: 'italic', checked: style.italic }, [], { change: onChange }),
        this.t(browser.i18n.getMessage('italic'))
      ]),
      this.h('label', {}, [
        this.t(browser.i18n.getMessage('size')),
        this.h('input', { type: 'number', step: '0.05', name: 'fontSize', value: style.size }, [], { change: onChange })
      ]),
      this.h('label', {}, [
        this.t(browser.i18n.getMessage('decoration')),
        this.h('select', { value: style.decoration, name: 'decoration' }, [
          this.h('option', { value: 'none', selected: style.decoration === 'none' }, [
            this.t(browser.i18n.getMessage('noDecoration'))
          ]),
          this.h('option', { value: 'underline', selected: style.decoration === 'underline' }, [
            this.t(browser.i18n.getMessage('underline'))
          ]),
          this.h('option', { value: 'line-through', selected: style.decoration === 'line-through' }, [
            this.t(browser.i18n.getMessage('strikethrough'))
          ])
        ], { change: onChange })
      ]),
      example
    ])
  },

  h: function (tag, props, children, events) {
    if (!props) props = {}
    if (!children) children = []
    if (!events) events = {}

    const elt = document.createElement(tag)
    Object.assign(elt, props)
    for (const ch of children) {
      elt.appendChild(ch)
    }

    for (const evt in events) {
      elt.addEventListener(evt, e => events[evt](props.name, elt.type === 'checkbox' ? elt.checked : elt.value, e))
    }

    return elt
  },

  t: txt => document.createTextNode(txt),

  intl: () => document.querySelectorAll('*[data-message]').forEach(elt => { elt.innerHTML = browser.i18n.getMessage(elt.dataset.message) })
}
