const idsToDomain = {}

browser.runtime.onMessage.addListener(msg => {
  if (msg.message === 'notification') {
    idsToDomain[browser.notifications.create(msg.data)] = msg.domain
  }
})

browser.notifications.onButtonClicked(id => {
  browser.tabs.create({
    url: `https://alternativeto.net/browse/search?q=${idsToDomain[id]}`
  })
})
