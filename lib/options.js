function onChange (style) {
  return (example, prop, val) => {
    browser.storage.local.get({
      mainStyle: utils.defaultStyle,
      visitedStyle: null
    }).then(res => {
      const stl = (!res.visitedStyle || style === 'mainStyle')
        ? res.mainStyle
        : res.visitedStyle

      stl[prop] = val
      browser.storage.local.set({ [style]: stl })

      const [ sProp, sVal ] = utils.confToStyle(prop, val)
      example.style[sProp] = sVal
    })
  }
}

function restoreOptions () {
  utils.intl()
  document.getElementById('domain').placeholder = browser.i18n.getMessage('domain')

  utils.getDefaultDomains().then(defaultDomains => {
    browser.storage.local.get({
      mainStyle: utils.defaultStyle,
      visitedStyle: null,
      useVisitedStyle: false,
      domains: defaultDomains,
      showTooltip: false
    }).then(result => {
      result.domains = orderDomains(result.domains)
      let disposeForm = buildForms(result)

      document.getElementById('reset-style').addEventListener('click', evt => {
        disposeForm()
        disposeForm = buildForms(result)
        browser.storage.local.set({
          mainStyle: result.mainStyle,
          visitedStyle: result.visitedStyle,
          useVisitedStyle: result.useVisitedStyle
        })
      })

      const tooltipBox = document.getElementById('tooltip')
      tooltipBox.checked = result.showTooltip
      tooltipBox.addEventListener('change', evt => {
        browser.storage.local.set({ showTooltip: tooltipBox.checked })
      })

      result.domains.forEach(createDomainLi)
      document.getElementById('add-domain').addEventListener('submit', evt => {
        evt.preventDefault()
        const domain = document.getElementById('domain').value.replace(/(https?:\/\/)?(www.)?/, '').split('/')[0]
        createDomainLi(domain, result.domains.length, result.domains)
        result.domains.push(domain)
        browser.storage.local.set({ domains: result.domains })
        document.getElementById('domain').value = ''
      })
    }).catch(console.error)

    document.getElementById('import').addEventListener('click', handleImport())

    document.getElementById('export').addEventListener('click', handleExport(defaultDomains))

    document.getElementById('reset-list').addEventListener('click', evt => {
      const list = document.getElementById('domain-list')
      while (list.firstChild) {
        list.removeChild(list.firstChild)
      }

      defaultDomains.forEach(createDomainLi)

      browser.storage.local.set({ domains: defaultDomains })
    })
  })
}

function orderDomains (domains) {
  return domains.sort((_a, _b) => {
    const normalA = _a.split('.')
    const normalB = _b.split('.')
    normalA.pop()
    normalB.pop()

    const a = normalA.reverse()
    const b = normalB.reverse()

    let i = 0
    while (a[i] && b[i]) {
      if (a[i] > b[i]) {
        return 1
      } else if (a[i] < b[i]) {
        return b
      }
      i++
    }
    return 0
  })
}

function buildForms (result) {
  const styleOptions = document.getElementById('style-options')
  const mainStyleForm = utils.buildStyleForm(browser.i18n.getMessage('normalLinks'), onChange('mainStyle'), result.mainStyle)
  const visitedForm = utils.buildStyleForm(browser.i18n.getMessage('visitedLinks'), onChange('visitedStyle'), result.visitedStyle || result.mainStyle)
  const secondStyleBox = utils.h('label', {}, [
    utils.t(browser.i18n.getMessage('useVisitedStyle')),
    utils.h('input', { type: 'checkbox', checked: result.useVisitedStyle }, [], {
      change: (_, checked) => {
        if (checked) {
          visitedForm.style.display = 'block'
          browser.storage.local.set({ useVisitedStyle: true })
        } else {
          visitedForm.style.display = 'none'
          browser.storage.local.set({ useVisitedStyle: false })
        }
      }
    })
  ])
  styleOptions.appendChild(secondStyleBox)

  styleOptions.appendChild(mainStyleForm)

  visitedForm.style.display = result.useVisitedStyle ? 'block' : 'none'
  styleOptions.appendChild(visitedForm)

  return () => {
    styleOptions.removeChild(mainStyleForm)
    styleOptions.removeChild(secondStyleBox)
    styleOptions.removeChild(visitedForm)
  }
}

function handleImport () {
  return evt => {
    const fileInput = utils.h('input', {
      style: {
        display: 'none'
      },
      type: 'file',
      accept: '.json',
      acceptCharset: 'utf-8'
    },
    [],
    {
      change: evt => {
        if (fileInput.value !== fileInput.initialValue) {
          document.body.style.cursor = 'wait'

          const file = fileInput.files[0]
          const fReader = new FileReader()
          fReader.addEventListener('loadend', event => {
            fileInput.remove()
            const importedDomains = JSON.parse(event.target.result).domains
            browser.storage.local.get({ domains: [] }).then(res => {
              const newDomains = importedDomains.filter(d => !res.domains.includes(d))
              const domains = newDomains.concat(res.domains)

              const list = document.getElementById('domain-list')
              while (list.firstChild) {
                list.removeChild(list.firstChild)
              }

              domains.forEach(createDomainLi)
              document.body.style.cursor = ''

              browser.storage.local.set({ domains })
            })
          })
          fReader.readAsText(file, 'utf-8')
        }
      }
    })
    fileInput.initialValue = fileInput.value
    document.body.appendChild(fileInput)
    fileInput.click()
  }
}

function handleExport (defaultDomains) {
  return evt => {
    browser.storage.local.get({ domains: defaultDomains }).then(res => {
      const data = JSON.stringify(res)
      const blob = new Blob([ data ], { type: 'octet/stream' })
      const objectURL = URL.createObjectURL(blob)
      browser.downloads.download({
        url: objectURL,
        filename: 'gafalink-domains.json'
      })
    })
  }
}

function createDomainLi (domain, i, domains) {
  const li = utils.h('li', {}, [
    utils.h('p', {}, [ utils.t(domain) ]),
    utils.h('a', { href: '#' }, [
      utils.t(browser.i18n.getMessage('delete'))
    ], {
      click: (p, v, evt) => {
        evt.preventDefault()
        document.getElementById('domain-list').removeChild(li)
        browser.storage.local.set({ domains: domains.filter(d => d !== domain) })
      }
    })
  ])
  document.getElementById('domain-list').appendChild(li)
}

document.addEventListener('DOMContentLoaded', restoreOptions)
