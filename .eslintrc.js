module.exports = {
    extends: 'standard',
    env: {
      browser: true
    },
    globals: {
      browser: true,
      utils: true
    }
}
