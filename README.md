# GAFALink

This small extension will highlight links to proprietary services online. Then, it will be easier for you
to avoid visiting them, and remember to prefer libre alternatives.

Thanks to **valvin**, **flynn**, **Florestan**, **mmu_man**, **drulac**, **gildas** and
who contributed to the list of domains to highlight.

And thanks to **Quadragondin** who designed the beautiful icon (the font used is "League Gothic Italic").

## Contributing

- Fork this repo
- Clone your fork on your computer
- `npm i --dev`
- Start working on a new branch
- Prefer small commits
- Once you are done, push to your fork
- Open a Merge Request

To test your changes, I would advice you to use the `web-ext` tool. To install it:

```
npm i -g web-ext
```

Then, to run your extension

```
web-ext run
```
## Translating

If you want to translate, create a new folder in the `_locales` folder named after the short code of your langage (e.g `fr` for French),
and copy `_locales/en/messages.json` inside. Then, simply edit the `message` property of each object of the file.

## Download it!

You can download it from [the official store](https://addons.mozilla.org/firefox/addon/gafalink/), or get the latest version with the build artifacts.
